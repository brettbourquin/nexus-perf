/*
 * Copyright (c) 2007-2013 Sonatype, Inc. All rights reserved.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0,
 * which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html.
 */
package com.sonatype.nexus.perftest.maven;

import java.io.File;
import java.util.Random;

import com.sonatype.nexus.perftest.ClientSwarm.ClientRequestInfo;
import com.sonatype.nexus.perftest.ClientSwarm.Operation;
import com.sonatype.nexus.perftest.Nexus;
import com.sonatype.nexus.perftest.operation.AbstractNexusOperation;

import org.sonatype.nexus.client.core.subsystem.repository.HostedRepository;
import org.sonatype.nexus.client.core.subsystem.repository.Repositories;

import com.codahale.metrics.Meter;
import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Deploys/Redeploys range of versions of artifacts to an existing repository over and over again. The target repository
 * must have redeploy enabled.
 */
public class RepositoryReDeployOperation
    extends AbstractNexusOperation
    implements Operation
{
  private final File pomTemplate;

  private final File jarTemplate;

  private final String groupId;

  private final String artifactId;

  private final int versionCount;

  private final HostedRepository repository;

  private final Random random;

  @JsonCreator
  public RepositoryReDeployOperation(@JacksonInject Nexus nexus,
                                     @JsonProperty("pomTemplate") File pomTemplate,
                                     @JsonProperty("jarTemplate") File jarTemplate,
                                     @JsonProperty(value = "groupId", required = false) String groupId,
                                     @JsonProperty(value = "artifactId", required = false) String artifactId,
                                     @JsonProperty(value = "versionCount", defaultValue = "100") int versionCount,
                                     @JsonProperty("repo") String repo)
  {
    super(nexus);
    this.pomTemplate = pomTemplate;
    this.jarTemplate = jarTemplate;
    this.groupId = groupId == null ? "test.group" : groupId;
    this.artifactId = artifactId == null ? "artifact" : artifactId;
    this.versionCount = versionCount;
    this.repository = getNexusClient(newRepositoryFactories()).getSubsystem(Repositories.class)
        .get(HostedRepository.class, repo);
    this.random = new Random();
  }

  @Override
  public void perform(ClientRequestInfo requestInfo) throws Exception {
    ArtifactDeployer artifactDeployer = new ArtifactDeployer(requestInfo.getHttpClient(), repository.contentUri());
    String version = String.valueOf(random.nextInt(versionCount) + 1);
    Meter uploadedBytesMeter = requestInfo.getContextValue("metric.uploadedBytesMeter");
    long uploaded = 0;
    uploaded += artifactDeployer.deployPom(groupId, artifactId, version, pomTemplate);
    uploaded += artifactDeployer.deployJar(groupId, artifactId, version, jarTemplate);
    uploadedBytesMeter.mark(uploaded);
  }
}
