package com.sonatype.nexus.perftest.config.client;

import org.sonatype.nexus.client.core.subsystem.repository.HostedRepository;

public interface HelmHostedRepository
    extends HostedRepository<HelmHostedRepository>
{
}
