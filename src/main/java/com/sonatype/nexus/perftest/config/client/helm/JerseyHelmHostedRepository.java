package com.sonatype.nexus.perftest.config.client.helm;

import com.sonatype.nexus.perftest.config.client.HelmHostedRepository;

import org.sonatype.nexus.client.internal.rest.jersey.subsystem.repository.JerseyHostedRepository;
import org.sonatype.nexus.client.rest.jersey.JerseyNexusClient;
import org.sonatype.nexus.rest.model.RepositoryResource;

public class JerseyHelmHostedRepository
    extends JerseyHostedRepository<HelmHostedRepository>
    implements HelmHostedRepository
{
  static final String PROVIDER_ROLE = "org.sonatype.nexus.proxy.repository.Repository";

  static final String PROVIDER = "helm-hosted";

  public JerseyHelmHostedRepository(final JerseyNexusClient nexusClient, final String id) {
    super(nexusClient, id);
  }

  public JerseyHelmHostedRepository(final JerseyNexusClient nexusClient,
                                    final RepositoryResource settings)
  {
    super(nexusClient, settings);
  }

  @Override
  protected RepositoryResource createSettings() {
    final RepositoryResource settings = super.createSettings();

    settings.setProviderRole(JerseyHelmHostedRepository.PROVIDER_ROLE);
    settings.setProvider(JerseyHelmHostedRepository.PROVIDER);
    settings.setRepoPolicy("RELEASE");
    settings.setIndexable(false);

    return settings;
  }
}
