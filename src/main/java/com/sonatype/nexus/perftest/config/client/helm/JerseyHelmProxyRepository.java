package com.sonatype.nexus.perftest.config.client.helm;

import com.sonatype.nexus.perftest.config.client.HelmProxyRepository;

import org.sonatype.nexus.client.internal.rest.jersey.subsystem.repository.JerseyProxyRepository;
import org.sonatype.nexus.client.rest.jersey.JerseyNexusClient;
import org.sonatype.nexus.rest.model.RepositoryProxyResource;

public class JerseyHelmProxyRepository
    extends JerseyProxyRepository<HelmProxyRepository>
    implements HelmProxyRepository
{
  static final String PROVIDER_ROLE = "org.sonatype.nexus.proxy.repository.Repository";

  static final String PROVIDER = "helm-proxy";

  public JerseyHelmProxyRepository(final JerseyNexusClient nexusClient, final String id) {
    super(nexusClient, id);
  }

  public JerseyHelmProxyRepository(final JerseyNexusClient nexusClient,
                                   final RepositoryProxyResource settings)
  {
    super(nexusClient, settings);
  }

  @Override
  protected RepositoryProxyResource createSettings() {
    final RepositoryProxyResource settings = super.createSettings();

    settings.setProviderRole(JerseyHelmProxyRepository.PROVIDER_ROLE);
    settings.setProvider(JerseyHelmProxyRepository.PROVIDER);
    settings.setRepoPolicy("RELEASE");
    settings.setIndexable(false);

    return settings;
  }
}
