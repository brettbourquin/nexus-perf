package com.sonatype.nexus.perftest.config.client.helm;

import com.sonatype.nexus.perftest.config.client.HelmProxyRepository;

import org.sonatype.nexus.client.core.subsystem.repository.Repository;
import org.sonatype.nexus.client.internal.rest.jersey.subsystem.repository.JerseyProxyRepositoryFactory;
import org.sonatype.nexus.client.rest.jersey.JerseyNexusClient;
import org.sonatype.nexus.rest.model.RepositoryBaseResource;
import org.sonatype.nexus.rest.model.RepositoryProxyResource;

public class JerseyHelmProxyRepositoryFactory
    extends JerseyProxyRepositoryFactory
{
  @Override
  public int canAdapt(final RepositoryBaseResource resource) {
    int score = super.canAdapt(resource);
    if (score > 0) {
      if (JerseyHelmProxyRepository.PROVIDER_ROLE.equals(resource.getProviderRole()) &&
          JerseyHelmProxyRepository.PROVIDER.equals(resource.getProvider())) {
        score++;
      }
    }
    return score;
  }

  @Override
  public JerseyHelmProxyRepository adapt(final JerseyNexusClient nexusClient,
                                         final RepositoryBaseResource resource)
  {
    return new JerseyHelmProxyRepository(nexusClient, (RepositoryProxyResource) resource);
  }

  @Override
  public boolean canCreate(final Class<? extends Repository> type) {
    return HelmProxyRepository.class.equals(type);
  }

  @Override
  public JerseyHelmProxyRepository create(final JerseyNexusClient nexusClient, final String id) {
    return new JerseyHelmProxyRepository(nexusClient, id);
  }

}
