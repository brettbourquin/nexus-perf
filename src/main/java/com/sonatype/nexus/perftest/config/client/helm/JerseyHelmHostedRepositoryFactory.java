package com.sonatype.nexus.perftest.config.client.helm;

import com.sonatype.nexus.perftest.config.client.HelmHostedRepository;

import org.sonatype.nexus.client.core.subsystem.repository.Repository;
import org.sonatype.nexus.client.internal.rest.jersey.subsystem.repository.JerseyHostedRepositoryFactory;
import org.sonatype.nexus.client.rest.jersey.JerseyNexusClient;
import org.sonatype.nexus.rest.model.RepositoryBaseResource;
import org.sonatype.nexus.rest.model.RepositoryResource;

public class JerseyHelmHostedRepositoryFactory
    extends JerseyHostedRepositoryFactory
{
  @Override
  public int canAdapt(final RepositoryBaseResource resource) {
    int score = super.canAdapt(resource);
    if (score > 0) {
      if (JerseyHelmHostedRepository.PROVIDER_ROLE.equals(resource.getProviderRole()) &&
          JerseyHelmHostedRepository.PROVIDER.equals(resource.getProvider())) {
        score++;
      }
    }
    return score;
  }

  @Override
  public JerseyHelmHostedRepository adapt(final JerseyNexusClient nexusClient,
                                          final RepositoryBaseResource resource)
  {
    return new JerseyHelmHostedRepository(nexusClient, (RepositoryResource) resource);
  }

  @Override
  public boolean canCreate(final Class<? extends Repository> type) {
    return HelmHostedRepository.class.equals(type);
  }

  @Override
  public JerseyHelmHostedRepository create(final JerseyNexusClient nexusClient, final String id) {
    return new JerseyHelmHostedRepository(nexusClient, id);
  }

}
