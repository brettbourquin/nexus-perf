package com.sonatype.nexus.perftest.config.client;

import org.sonatype.nexus.client.core.subsystem.repository.ProxyRepository;

public interface HelmProxyRepository
    extends ProxyRepository<HelmProxyRepository>
{
}
