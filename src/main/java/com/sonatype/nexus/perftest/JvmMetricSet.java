package com.sonatype.nexus.perftest;

import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricSet;
import com.codahale.metrics.jvm.BufferPoolMetricSet;
import com.codahale.metrics.jvm.ClassLoadingGaugeSet;
import com.codahale.metrics.jvm.FileDescriptorRatioGauge;
import com.codahale.metrics.jvm.GarbageCollectorMetricSet;
import com.codahale.metrics.jvm.MemoryUsageGaugeSet;
import com.codahale.metrics.jvm.ThreadStatesGaugeSet;
import com.google.common.collect.ImmutableMap;

import javax.management.MBeanServer;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * JVM {@link MetricSet}.
 */
public class JvmMetricSet
  implements MetricSet
{
  private final MBeanServer server;

  public JvmMetricSet(final MBeanServer server) {
    this.server = checkNotNull(server);
  }

  @Override
  public Map<String, Metric> getMetrics() {
    Map<String, Metric> metrics = new HashMap<>();

    metrics.put("buffer-pool", new BufferPoolMetricSet(server));
    metrics.put("class-loading", new ClassLoadingGaugeSet());
    metrics.put("fd", new FileDescriptorRatioGauge());
    metrics.put("gc", new GarbageCollectorMetricSet());
    metrics.put("memory-usage", new MemoryUsageGaugeSet());
    metrics.put("thread-states", new ThreadStatesGaugeSet());

    return ImmutableMap.copyOf(metrics);
  }
}
