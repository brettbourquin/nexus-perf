package com.sonatype.nexus.perftest;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Overrides {

    private static final Pattern NUMBER_OF_CLIENTS_PATTERN = Pattern.compile("test\\.(.*)\\.numberOfClients");
    private static final Pattern RATE_PATTERN = Pattern.compile("test\\.(.*)\\.rate");

    private final Duration duration;
    private final Integer globalNumberOfClients;
    private final Map<String, Integer> numberOfClients;
    private final String globalRate;
    private final Map<String, String> rates;
    private final Integer globalRetainFailures;

    public Overrides() {
        String s = System.getProperty("test.duration");
        this.duration = s != null ? new Duration(s) : null;

        // ---

        s = System.getProperty("test.numberOfClients");
        this.globalNumberOfClients = s != null ? Integer.parseInt(s) : null;

        // ---

        this.globalRate = System.getProperty("test.rate");

        // ---

        s = System.getProperty("test.retainFailures");
        this.globalRetainFailures = s != null ? Integer.parseInt(s) : null;

        // ---

        Map<String, Integer> nocs = new HashMap<>();
        Map<String, String> rates = new HashMap<>();

        System.getProperties().forEach((k, v) -> {
            String ks = k.toString();

            Matcher m = NUMBER_OF_CLIENTS_PATTERN.matcher(ks);
            if (m.matches()) {
                String testName = m.group(1);
                nocs.put(testName, Integer.parseInt(v.toString()));
            }

            m = RATE_PATTERN.matcher(ks);
            if (m.matches()) {
                String testName = m.group(1);
                rates.put(testName, v.toString());
            }
        });

        this.numberOfClients = nocs;
        this.rates = rates;
    }

    public Duration getDuration() {
        return duration;
    }

    public int getNumberOfClients(String testName, int defaultValue) {
        Integer i = numberOfClients.get(testName);
        if (i == null) {
            return globalNumberOfClients != null ? globalNumberOfClients : defaultValue;
        }
        return i;
    }

    public RequestRate getRate(String testName, RequestRate defaultValue) {
        String s = rates.get(testName);
        if (s == null) {
            return globalRate != null ? new RequestRate(globalRate) : defaultValue;
        }
        return new RequestRate(s);
    }

    public Integer getGlobalRetainFailures() {
        return globalRetainFailures;
    }
}
