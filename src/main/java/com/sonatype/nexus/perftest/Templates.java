package com.sonatype.nexus.perftest;

public class Templates {

    private final String artifactIdTemplate;

    public Templates() {
        this.artifactIdTemplate = System.getProperty("template.artifactId", "artifact-%s");
    }

    public String artifactId(int artifactNo, String operationId) {
        return String.format(artifactIdTemplate, operationId, artifactNo);
    }
}
