package com.sonatype.nexus.perftest.docker;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.sonatype.nexus.perftest.ClientSwarm.ClientRequestInfo;
import com.sonatype.nexus.perftest.ClientSwarm.Operation;
import com.sonatype.nexus.perftest.Nexus;
import com.sonatype.nexus.perftest.operation.AbstractNexusOperation;
import com.sonatype.nexus.perftest.paths.DownloadPaths;
import com.sonatype.nexus.perftest.paths.DownloadPaths.PathIterator;

import com.codahale.metrics.Meter;
import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.ByteStreams;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

public class DockerOperation
    extends AbstractNexusOperation
    implements Operation
{

  private static final ObjectMapper mapper = new ObjectMapper();

  private static final TypeReference<HashMap<String, Object>> MAP_TYPE_REFERENCE = new TypeReference<HashMap<String, Object>>() { };

  private static final Encoder BASE64_ENCODER = Base64.getEncoder();

  private final Collection<Action> actions;

  protected PathIterator imagesIterator;

  public DockerOperation(
      @JacksonInject final Nexus nexus,
      @JsonProperty(value = "images", required = true) final DownloadPaths images,
      @JsonProperty(value = "actions", required = true) final Collection<Action> actions)
  {
    super(nexus);
    this.imagesIterator = images.circularIterator();
    this.actions = actions;
  }

  @Override
  public void perform(final ClientRequestInfo requestInfo) throws Exception {
    perform(
        requestInfo.getHttpClient(),
        requestInfo.getContextValue("metric.downloadedBytesMeter"),
        requestInfo.getContextValue("metric.uploadedBytesMeter")
    );
  }

  public void perform(final HttpClient httpClient,
                      final Meter downloadedBytesMeter,
                      final Meter uploadedBytesMeter)
      throws Exception
  {
    String image = imagesIterator.next();

    String[] segments = image.split(":");
    String name = segments[0];
    String tag = "latest";
    if (segments.length > 1) {
      tag = segments[1];
    }

    for (Action action : actions) {
      String url = action.url;
      if (url == null) {
        url = nexusBaseurl + "repository/" + action.repo;
      }
      log(action.type, name, tag, url);
      if ("pull".equals(action.type)) {
        pull(httpClient, action.cacheDir, downloadedBytesMeter, name, tag, url);
      }
      else if ("push".equals(action.type)) {
        push(httpClient, action.cacheDir, uploadedBytesMeter, name, tag, url);
      }
      else {
        throw new Exception("Unsupported action:" + action.type);
      }
    }
  }

  @Override
  public void close() throws Exception {
    imagesIterator.close();
  }

  protected void log(final String action, final String name, final String tag, final String url) {
    log.debug("{} {}:{} from/to {}", action, name, tag, url);
  }

  private void pull(final HttpClient httpClient,
                    final File cacheDir,
                    final Meter meter,
                    final String name,
                    final String tag,
                    final String registryUrl)
      throws Exception
  {
    long downloaded = 0;
    Header auth = getToken(httpClient, registryUrl, name);
    File manifestFile = new File(cacheDir, name + "/" + tag + "/manifest.json");
    if (cacheDir == null || !manifestFile.exists()) {
      HttpGet manifestRequest = new HttpGet(registryUrl + "/v2/" + name + "/manifests/" + tag);
      manifestRequest.addHeader(auth);
      manifestRequest.addHeader(HttpHeaders.ACCEPT, "application/vnd.docker.distribution.manifest.v2+json");
      HttpResponse manifestResponse = httpClient.execute(manifestRequest);
      if (manifestResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
        EntityUtils.consumeQuietly(manifestResponse.getEntity());
        throw new IOException(
            "Failed to pull manifest (" + manifestResponse.getStatusLine().getStatusCode() + "): "
                + name + ":" + tag
        );
      }
      ByteArrayOutputStream manifestBaos = new ByteArrayOutputStream();
      downloaded += ByteStreams.copy(manifestResponse.getEntity().getContent(), manifestBaos);
      EntityUtils.consumeQuietly(manifestResponse.getEntity());
      Map<String, Object> manifest = mapper.readValue(manifestBaos.toByteArray(), MAP_TYPE_REFERENCE);
      if (!Objects.equals(manifest.get("schemaVersion"), 2)) {
        throw new IOException(
            "Unsupported manifest version (" + manifest.get("schemaVersion") + "): " + name + ":" + tag
        );
      }
      if (cacheDir != null) {
        dump(new ByteArrayInputStream(manifestBaos.toByteArray()), manifestFile);
      }
      Map<String, Object> config = (Map<String, Object>) manifest.get("config");
      if (config != null) {
        downloaded += pullBlob(httpClient, cacheDir, registryUrl, name, tag, auth, config);
      }
      List<Map<String, Object>> layers = (List<Map<String, Object>>) manifest.get("layers");
      for (Map<String, Object> layer : layers) {
        downloaded += pullBlob(httpClient, cacheDir, registryUrl, name, tag, auth, layer);
      }
    }
    if (meter != null) {
      meter.mark(downloaded);
    }
  }

  private long pullBlob(final HttpClient httpClient,
                        final File cacheDir,
                        final String registryUrl,
                        final String name,
                        final String tag,
                        final Header auth,
                        final Map<String, Object> blobRef) throws Exception
  {
    String digest = (String) blobRef.get("digest");
    File blobFile = new File(cacheDir, name + "/" + tag + "/" + digest.replace(":", "_"));
    if (cacheDir == null || !blobFile.exists()) {
      HttpGet blobRequest = new HttpGet(registryUrl + "/v2/" + name + "/blobs/" + digest);
      blobRequest.addHeader(auth);
      blobRequest.addHeader(HttpHeaders.ACCEPT, (String) blobRef.get("mediaType"));
      HttpResponse blobResponse = httpClient.execute(blobRequest);
      try {
        if (blobResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
          EntityUtils.consumeQuietly(blobResponse.getEntity());
          throw new IOException(
              "Failed to pull blob (" + blobResponse.getStatusLine().getStatusCode() + "): "
                  + name + ":" + tag + " " + digest
          );
        }
        if (cacheDir != null) {
          return dump(blobResponse.getEntity().getContent(), blobFile);
        }
        else {
          // pull all content (we just don't write it to a file)
          try (InputStream in = blobResponse.getEntity().getContent()) {
            return ByteStreams.copy(in, ByteStreams.nullOutputStream());
          }
        }
      }
      finally {
        EntityUtils.consumeQuietly(blobResponse.getEntity());
      }
    }
    return 0;
  }

  private void push(final HttpClient httpClient,
                    final File cacheDir,
                    final Meter meter,
                    final String name,
                    final String tag,
                    final String registryUrl)
      throws Exception
  {
    long uploaded = 0;
    Header auth = getToken(httpClient, registryUrl, name);
    File manifestFile = new File(cacheDir, name + "/" + tag + "/manifest.json");
    Map<String, Object> manifest;
    try (InputStream in = new BufferedInputStream(new FileInputStream(manifestFile))) {
      manifest = mapper.readValue(in, MAP_TYPE_REFERENCE);
    }
    if (!Objects.equals(manifest.get("schemaVersion"), 2)) {
      throw new IOException(
          "Unsupported manifest version (" + manifest.get("schemaVersion") + "): " + name + ":" + tag
      );
    }
    Map<String, Object> config = (Map<String, Object>) manifest.get("config");
    if (config != null) {
      uploaded += pushBlob(httpClient, cacheDir, registryUrl, name, tag, auth, config);
    }
    List<Map<String, Object>> layers = (List<Map<String, Object>>) manifest.get("layers");
    for (Map<String, Object> layer : layers) {
      uploaded += pushBlob(httpClient, cacheDir, registryUrl, name, tag, auth, layer);
    }
    HttpPut manifestRequest = new HttpPut(registryUrl + "/v2/" + name + "/manifests/" + tag);
    manifestRequest.addHeader(auth);
    manifestRequest.setEntity(new FileEntity(manifestFile, ContentType.create((String) manifest.get("mediaType"))));
    HttpResponse manifestResponse = httpClient.execute(manifestRequest);
    EntityUtils.consumeQuietly(manifestResponse.getEntity());
    if (manifestResponse.getStatusLine().getStatusCode() != HttpStatus.SC_CREATED) {
      throw new IOException(
          "Failed to push manifest (" + manifestResponse.getStatusLine().getStatusCode() + "): "
              + name + ":" + tag
      );
    }
    uploaded += manifestFile.length();
    if (meter != null) {
      meter.mark(uploaded);
    }
  }

  private long pushBlob(final HttpClient httpClient,
                        final File cacheDir,
                        final String registryUrl,
                        final String name,
                        final String tag,
                        final Header auth,
                        final Map<String, Object> blobRef) throws Exception
  {
    String digest = (String) blobRef.get("digest");

    HttpPost startUploadRequest = new HttpPost(registryUrl + "/v2/" + name + "/blobs/uploads/");
    startUploadRequest.addHeader(auth);
    HttpResponse startUploadResponse = httpClient.execute(startUploadRequest);
    EntityUtils.consumeQuietly(startUploadResponse.getEntity());
    if (startUploadResponse.getStatusLine().getStatusCode() != HttpStatus.SC_ACCEPTED) {
      throw new IOException(
          "Failed to upload (" + startUploadResponse.getStatusLine().getStatusCode() + "): " + digest
      );
    }
    String uploadLocation = startUploadResponse.getFirstHeader("location").getValue();
    if (!uploadLocation.startsWith("http")) {
      uploadLocation = registryUrl + uploadLocation;
    }
    HttpPatch uploadRequest = new HttpPatch(uploadLocation);
    uploadRequest.addHeader(auth);
    File uploadFile = new File(cacheDir, name + "/" + tag + "/" + digest.replace(":", "_"));
    uploadRequest.setEntity(new FileEntity(uploadFile));
    HttpResponse uploadResponse = httpClient.execute(uploadRequest);
    EntityUtils.consumeQuietly(uploadResponse.getEntity());
    if (uploadResponse.getStatusLine().getStatusCode() != HttpStatus.SC_ACCEPTED) {
      throw new IOException(
          "Failed to upload (" + uploadResponse.getStatusLine().getStatusCode() + "): " + digest
      );
    }
    String finishUploadLocation = uploadResponse.getFirstHeader("location").getValue();
    if (!finishUploadLocation.startsWith("http")) {
      finishUploadLocation = registryUrl + finishUploadLocation;
    }
    finishUploadLocation = new URIBuilder(finishUploadLocation).addParameter("digest", digest).build().toASCIIString();
    HttpPut finishUploadRequest = new HttpPut(finishUploadLocation);
    finishUploadRequest.setHeader(auth);
    HttpResponse finishUploadResponse = httpClient.execute(finishUploadRequest);
    EntityUtils.consumeQuietly(finishUploadResponse.getEntity());
    if (finishUploadResponse.getStatusLine().getStatusCode() != HttpStatus.SC_CREATED) {
      throw new IOException(
          "Failed to upload (" + finishUploadResponse.getStatusLine().getStatusCode() + "): " + digest
      );
    }
    return uploadFile.length();
  }

  private long dump(final InputStream in, final File file) throws IOException {
    file.getParentFile().mkdirs();
    try (BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file))) {
      return ByteStreams.copy(in, out);
    }
  }

  private Header getToken(final HttpClient httpClient, final String registryUrl, final String name)
      throws Exception
  {
    HttpResponse response = httpClient.execute(new HttpGet(registryUrl + "/v2/"));
    EntityUtils.consume(response.getEntity());
    if (response.getStatusLine().getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
      Header wwwAuth = response.getFirstHeader(HttpHeaders.WWW_AUTHENTICATE);
      if (wwwAuth.getValue().startsWith("Bearer")) {
        Map<String, String> authElements = new HashMap<>();
        HeaderElement[] elements = wwwAuth.getElements();
        for (HeaderElement element : elements) {
          authElements.put(element.getName(), element.getValue());
        }
        String realm = authElements.get("Bearer realm");
        HttpGet request = new HttpGet(
            realm + "?service=" + authElements.get("service") + "&scope=repository:" + name + ":pull,push"
        );
        if (realm.startsWith(nexusBaseurl)) {
          String auth = BASE64_ENCODER.encodeToString((nexus.getUsername() + ":" + nexus.getPassword()).getBytes());
          request.setHeader(HttpHeaders.AUTHORIZATION, "Basic " + auth);
        }
        HttpResponse authResponse = httpClient.execute(request);
        if (authResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
          HashMap<String, Object> value = mapper.readValue(authResponse.getEntity().getContent(), MAP_TYPE_REFERENCE);
          EntityUtils.consume(authResponse.getEntity());
          return new BasicHeader(HttpHeaders.AUTHORIZATION, "Bearer " + value.get("token"));
        }
        EntityUtils.consume(authResponse.getEntity());
        throw new IOException(
            "Failed to retrieve authentication token (" + authResponse.getStatusLine().getStatusCode() + "): " + realm
        );
      }
    }
    return null;
  }

  public static class Action
  {
    private final String type;

    private final String url;

    private final String repo;

    private final File cacheDir;

    public Action(@JsonProperty(value = "type", required = true) final String type,
                  @JsonProperty(value = "url") final String url,
                  @JsonProperty(value = "repo") final String repo,
                  @JsonProperty(value = "cacheDir") final File cacheDir)
    {
      this.type = type;
      this.url = url;
      this.repo = repo;
      this.cacheDir = cacheDir;
    }
  }
}
