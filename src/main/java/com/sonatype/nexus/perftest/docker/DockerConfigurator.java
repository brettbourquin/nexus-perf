package com.sonatype.nexus.perftest.docker;

import java.util.Collection;

import com.sonatype.nexus.perftest.Nexus;
import com.sonatype.nexus.perftest.PerformanceTest.NexusConfigurator;
import com.sonatype.nexus.perftest.paths.DownloadPaths;

import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

public class DockerConfigurator
    extends DockerOperation
    implements NexusConfigurator
{

  public DockerConfigurator(
      @JacksonInject final Nexus nexus,
      @JsonProperty(value = "images", required = true) final DownloadPaths images,
      @JsonProperty(value = "actions", required = true) final Collection<Action> actions) throws Exception
  {
    super(nexus, images, actions);
    imagesIterator.close();
    imagesIterator = images.iterator();
    try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
      while (imagesIterator.hasNext()) {
        try {
          perform(httpClient, null, null);
        }
        catch (Exception e) {
          log.error("Unexpected exception", e);
        }
      }
    }
  }

  @Override
  public void cleanup() throws Exception {
    close();
  }

  protected void log(final String action, final String name, final String tag, final String url) {
    log.info("{} {}:{} from/to {}", action, name, tag, url);
  }
}
