package com.sonatype.nexus.perftest.operation;

import java.io.Closeable;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import com.sonatype.nexus.perftest.ClientSwarm.ClientRequestInfo;
import com.sonatype.nexus.perftest.ClientSwarm.Operation;
import com.sonatype.nexus.perftest.Nexus;

import com.codahale.metrics.Meter;
import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.io.ByteStreams;
import com.google.common.io.CountingInputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.InputStreamEntity;

/**
 * Uploads files to specified repo/urls.
 */
public class UploadOperation
    extends AbstractNexusOperation
    implements Operation
{
  private final String userAgent;

  private final String baseUrl;

  private final Iterable<UploadEntity> uploadEntities;

  private final Iterator<UploadEntity> uploadEntityIterator;

  public UploadOperation(@JacksonInject Nexus nexus,
                         @JsonProperty("repo") String repo,
                         @JsonProperty(value = "userAgent") String userAgent,
                         @JsonProperty(value = "pathPrefix") String pathPrefix,
                         @JsonProperty(value = "uploadEntities", required = true) UploadEntities uploadEntities)
  {
    super(nexus);
    this.baseUrl = repo == null ? nexusBaseurl + (pathPrefix == null ? "" : pathPrefix) : getRepoBaseurl(repo);
    this.userAgent = userAgent;
    this.uploadEntities = uploadEntities;
    this.uploadEntityIterator = uploadEntities.iterator();
  }

  @Override
  public void perform(final ClientRequestInfo requestInfo) throws Exception {
    if (uploadEntityIterator.hasNext()) {
      UploadEntity uploadEntity = uploadEntityIterator.next();

      ByteStreams.copy(uploadEntity.getContent().get(), new FileOutputStream("/Users/adreghiciu/Downloads/foo.tgz"));

      String path = uploadEntity.getPath();
      if (path.startsWith("/")) {
        path = path.substring(1);
      }

      String url = baseUrl.endsWith("/") ? baseUrl + path : baseUrl + "/" + path;
      HttpPut method = new HttpPut(url);
      if (userAgent != null) {
        method.addHeader(HttpHeaders.USER_AGENT, userAgent);
      }

      CountingInputStream counter = null;
      try (InputStream in = uploadEntity.getContent().get()) {
        method.setEntity(new InputStreamEntity(counter = new CountingInputStream(in)));

        HttpResponse response = requestInfo.getHttpClient().execute(method);
        HttpEntity responseEntity = response.getEntity();
        if (responseEntity != null && responseEntity.isStreaming()) {
          try (CountingInputStream entityIn = new CountingInputStream(responseEntity.getContent())) {
            ByteStreams.copy(entityIn, ByteStreams.nullOutputStream());
            ((Meter) requestInfo.getContextValue("metric.downloadedBytesMeter")).mark(entityIn.getCount());
          }
        }
        if (!isSuccess(response)) {
          throw new IOException(response.getStatusLine().toString());
        }
      }
      finally {
        if (counter != null) {
          ((Meter) requestInfo.getContextValue("metric.uploadedBytesMeter")).mark(counter.getCount());
        }
      }
    }
  }

  @Override
  public void close() throws Exception {
    if (uploadEntities instanceof Closeable) {
      ((Closeable) uploadEntities).close();
    }
  }
}
