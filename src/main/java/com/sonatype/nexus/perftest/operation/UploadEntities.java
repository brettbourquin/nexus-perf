package com.sonatype.nexus.perftest.operation;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * A set of entities.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public interface UploadEntities
    extends Iterable<UploadEntity>
{
}
