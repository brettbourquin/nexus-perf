package com.sonatype.nexus.perftest.operation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.function.Supplier;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Throwables;

/**
 * An entity to be uploaded.
 */
public class UploadEntity
{
  private final String path;

  private final Supplier<InputStream> contentSupplier;

  public UploadEntity(final String path, final Supplier<InputStream> contentSupplier)
  {
    this.path = path;
    this.contentSupplier = contentSupplier;
  }

  public UploadEntity(
      @JsonProperty(value = "path", required = true) String path,
      @JsonProperty(value = "file") File contentSupplier)
  {
    this.path = path;
    this.contentSupplier = () -> {
      try {
        return new FileInputStream(contentSupplier);
      }
      catch (FileNotFoundException e) {
        throw Throwables.propagate(e);
      }
    };
  }

  public String getPath() {
    return path;
  }

  public Supplier<InputStream> getContent() {
    return contentSupplier;
  }
}
