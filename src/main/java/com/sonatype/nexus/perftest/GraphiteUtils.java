package com.sonatype.nexus.perftest;

import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.graphite.Graphite;
import com.codahale.metrics.graphite.GraphiteReporter;
import com.google.common.base.Throwables;
import org.codehaus.plexus.interpolation.InterpolationException;
import org.codehaus.plexus.interpolation.MapBasedValueSource;
import org.codehaus.plexus.interpolation.StringSearchInterpolator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public final class GraphiteUtils {

  private static final Logger log = LoggerFactory.getLogger(GraphiteUtils.class);
  public static final String GRAPHITE_HOST_KEY = "perftest.graphite.host";
  public static final String GRAPHITE_PREFIX_KEY = "perftest.graphite.prefix";
  public static final String DEFAULT_GRAPHITE_PREFIX = "devtools.nexus.testagent.${host-name}.perftest";

  public static GraphiteReporter startReporter(String graphiteHost, String prefix,
                                               MetricRegistry metricRegistry, String name, String suffix)
      throws InterpolationException {

    int graphitePort = Integer.valueOf(System.getProperty("perftest.graphite.port", "2003"));
    Duration graphiteFrequency = new Duration(System.getProperty("perftest.graphite.frequency", "1 MINUTES"));

    log.info(
        "Graphite reporter enabled for {}:{} every {} using prefix {}",
        graphiteHost, graphitePort, graphiteFrequency, prefix
    );

    Graphite graphite = new Graphite(new InetSocketAddress(graphiteHost, graphitePort));
    GraphiteReporter reporter = GraphiteReporter.forRegistry(metricRegistry)
        .prefixedWith(prefix)
        .convertRatesTo(TimeUnit.SECONDS)
        .convertDurationsTo(TimeUnit.MILLISECONDS)
        .filter(MetricFilter.ALL)
        .build(graphite);
    reporter.start(graphiteFrequency.getValue(), graphiteFrequency.getUnit());

    return reporter;
  }

  /**
   * Helper to interpolate given input.
   */
  public static String interpolate(final String input) throws InterpolationException {
    StringSearchInterpolator interpolator = new StringSearchInterpolator();
    interpolator.addValueSource(new MapBasedValueSource(tokens.get()));
    return interpolator.interpolate(input);
  }

  /**
   * Supplier of 'prefix' replacement tokens.
   */
  private static Supplier<Map<String, String>> tokens = (Supplier<Map<String, String>>) () -> {
    InetAddress localHost = null;
    try {
      localHost = InetAddress.getLocalHost();
    } catch (UnknownHostException e) {
      throw Throwables.propagate(e);
    }

    String hostName = localHost.getHostName();
    String canonicalHostName = localHost.getCanonicalHostName();

    Map<String, String> tokens = new HashMap<>();
    tokens.put("hostName", hostName);
    tokens.put("host-name", hostName.replace('.', '-'));
    tokens.put("host_name", hostName.replace('.', '_'));
    tokens.put("canonicalHostName", canonicalHostName);
    tokens.put("canonical-host-name", canonicalHostName.replace('.', '-'));
    tokens.put("canonical_host_name", canonicalHostName.replace('.', '_'));
    return tokens;
  };

  /**
   * Replace all non-word chars with '-'.
   */
  public static String clean(final String input) {
    return input.replaceAll("\\W", "-");
  }

  private GraphiteUtils() {
  }
}
