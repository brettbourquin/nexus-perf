package com.sonatype.nexus.perftest.npm;

import java.io.File;

import com.sonatype.nexus.perftest.paths.HttpdLogParser;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class NPMELogParser
    extends HttpdLogParser
{
  @JsonCreator
  public NPMELogParser(final @JsonProperty(value = "logfile", required = true) File logfile)
  {
    super(logfile, null);
  }

  @Override
  protected String parseLine(final String line) {
    return super.parseLine(line).substring(2).replace("_attachments", "-");
  }
}
