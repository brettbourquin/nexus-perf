/*
 * Copyright (c) 2007-2013 Sonatype, Inc. All rights reserved.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0,
 * which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html.
 */
package com.sonatype.nexus.perftest;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthState;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpCoreContext;

public class Nexus
{
  private static final int HTTP_TIMEOUT = Integer.parseInt(System.getProperty("perftest.http.timeout", "60000"));

  private final String baseurl;

  private final List<String> memberurls;

  private final String username;

  private final String password;

  public Nexus() {
    this.baseurl = System.getProperty("nexus.baseurl");
    this.username = System.getProperty("nexus.username");
    this.password = System.getProperty("nexus.password");
    this.memberurls = collectMemberUrls(System.getProperty("nexus.memberurls"));
  }

  @JsonCreator
  public Nexus(@JsonProperty("baseurl") String baseurl,
               @JsonProperty("username") String username,
               @JsonProperty("password") String password,
               @JsonProperty(value = "memberurls", required = false) String memberurls)
  {
    this.baseurl = baseurl;
    this.username = username;
    this.password = password;
    this.memberurls = collectMemberUrls(memberurls);
  }

  private List<String> collectMemberUrls(@Nullable final String memberUrlsString) {
    if (memberUrlsString != null) {
      return ImmutableList.copyOf(Splitter.on(',').omitEmptyStrings().split(memberUrlsString));
    }
    return null;
  }

  public String getBaseurl() {
    return baseurl;
  }

  public boolean isCluster() {
    return memberurls != null && !memberurls.isEmpty();
  }

  public List<String> getMemberurls() {
    if (memberurls != null) {
      return memberurls;
    }
    return Collections.emptyList();
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public static String sanitizeUrl(final String url) {
    String sanitizedUrl = url;
    if (sanitizedUrl == null || sanitizedUrl.trim().isEmpty()) {
      return null;
    }
    sanitizedUrl = sanitizedUrl.trim();
    return sanitizedUrl.endsWith("/") ? sanitizedUrl : (sanitizedUrl + "/");
  }

  public static CloseableHttpClient createHttpClient(final Nexus nexus, final boolean preemptiveAuth) {
    CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
    credentialsProvider.setCredentials(
        AuthScope.ANY, new UsernamePasswordCredentials(nexus.getUsername(), nexus.getPassword())
    );
    HttpClientBuilder httpClientBuilder = HttpClients.custom()
        .setConnectionManager(new BasicHttpClientConnectionManager())
        .setDefaultRequestConfig(
            RequestConfig.custom()
                .setConnectTimeout(HTTP_TIMEOUT)
                .setSocketTimeout(HTTP_TIMEOUT).build())
        .setDefaultCredentialsProvider(credentialsProvider);
    if (preemptiveAuth) {
      httpClientBuilder.addInterceptorFirst(new PreemptiveAuthInterceptor());
    }
    return httpClientBuilder.build();
  }

  private static final class PreemptiveAuthInterceptor
      implements HttpRequestInterceptor
  {
    public void process(final HttpRequest request, final HttpContext context) throws HttpException, IOException {
      AuthState authState = (AuthState) context.getAttribute(HttpClientContext.TARGET_AUTH_STATE);
      if (authState.getAuthScheme() == null) {
        CredentialsProvider credentialsProvider = (CredentialsProvider) context.getAttribute(
            HttpClientContext.CREDS_PROVIDER
        );
        HttpHost targetHost = (HttpHost) context.getAttribute(HttpCoreContext.HTTP_TARGET_HOST);
        AuthScope authScope = new AuthScope(targetHost.getHostName(), targetHost.getPort());
        Credentials credentials = credentialsProvider.getCredentials(authScope);
        authState.update(new BasicScheme(), credentials);
      }
    }
  }

}
