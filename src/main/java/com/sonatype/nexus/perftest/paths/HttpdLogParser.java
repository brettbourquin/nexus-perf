/*
 * Copyright (c) 2007-2013 Sonatype, Inc. All rights reserved.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0,
 * which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html.
 */
package com.sonatype.nexus.perftest.paths;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Throwables;

public class HttpdLogParser
    extends FileParser
    implements DownloadPaths
{
  private final String prefix;

  private final Date startTime;

  private final Date endTime;

  @JsonCreator
  public HttpdLogParser(final @JsonProperty(value = "logfile", required = true) File logfile,
                        final @JsonProperty(value = "prefix") String prefix,
                        final @JsonProperty(value = "startTime") String startTime,
                        final @JsonProperty(value = "endTime") String endTime)
  {
    super(logfile);
    this.prefix = prefix;
    this.startTime = parse(startTime);
    this.endTime = parse(endTime);
  }

  public HttpdLogParser(File logfile, String prefix)
  {
    this(logfile, prefix, null, null);
  }

  protected boolean isValidLine(final String line) {
    if (!line.contains("\"GET ")) {
      return false;
    }
    ;
    if (startTime != null || endTime != null) {
      StringTokenizer st = new StringTokenizer(line, "[]\" ");
      st.nextToken(); // ip
      st.nextToken(); // not sure
      st.nextToken(); // username
      String dateTime = st.nextToken(); // [date:time
      Date logDateTime = parse(dateTime);
      if (startTime != null && logDateTime.compareTo(startTime) < 0) {
        return false;
      }
      if (endTime != null && logDateTime.compareTo(endTime) > 0) {
        return false;
      }
    }
    return true;
  }

  protected String parseLine(final String line) {
    StringTokenizer st = new StringTokenizer(line, "[]\" ");
    st.nextToken(); // ip
    st.nextToken(); // not sure
    st.nextToken(); // username
    st.nextToken(); // [date:time
    st.nextToken(); // timezoneoffset]
    st.nextToken(); // "METHOD
    String path = st.nextToken(); // path
    if (prefix != null) {
      if (path.startsWith(prefix)) {
        return path.substring(prefix.length());
      }
    }
    return path;
  }

  private static Date parse(final String dateTime) {
    if (dateTime == null) {
      return null;
    }
    try {
      return new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss").parse(dateTime);
    }
    catch (Exception e) {
      throw Throwables.propagate(e);
    }
  }
}
