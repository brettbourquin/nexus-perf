/*
 * Copyright (c) 2007-2013 Sonatype, Inc. All rights reserved.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0,
 * which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html.
 */
package com.sonatype.nexus.perftest.paths;

import java.io.File;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LineParser
    extends FileParser
    implements DownloadPaths
{
  @JsonCreator
  public LineParser(final @JsonProperty(value = "file", required = true) File file)
  {
    super(file);
  }

  protected boolean isValidLine(final String line) {
    String trimmed = line.trim();
    return !trimmed.isEmpty() && !trimmed.startsWith("#");
  }

  protected String parseLine(final String line) {
    return line.trim();
  }
}
