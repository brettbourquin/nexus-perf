/*
 * Copyright (c) 2007-2013 Sonatype, Inc. All rights reserved.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0,
 * which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html.
 */
package com.sonatype.nexus.perftest.paths;

import java.io.Closeable;
import java.util.Iterator;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS, defaultImpl = HttpdLogParser.class, include = JsonTypeInfo.As.PROPERTY, property = "class")
@JsonSubTypes({
    @JsonSubTypes.Type(value = HttpdLogParser.class, name = "httpd"),
    @JsonSubTypes.Type(value = CsvLogParser.class, name = "csv")
})
public interface DownloadPaths
{
  PathIterator iterator();

  PathIterator circularIterator();

  interface PathIterator
      extends Iterator<String>, Closeable { }
}
