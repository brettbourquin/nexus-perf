/*
 * Copyright (c) 2007-2013 Sonatype, Inc. All rights reserved.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0,
 * which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html.
 */
package com.sonatype.nexus.perftest.paths;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Generated download paths that most probably does not exists on remote. Useful to exercise HttpClient and proxy
 * capabilities of Nexus.
 */
public class GeneratedDownloadPaths
    implements DownloadPaths
{
  private final String prefix;

  @JsonCreator
  public GeneratedDownloadPaths(@JsonProperty(value = "prefix", required = true) String prefix) {
    this.prefix = prefix;
  }

  @Override
  public PathIterator iterator() {
    throw new RuntimeException(getClass().getSimpleName() + "#iterator() cannot be invoked");
  }

  @Override
  public PathIterator circularIterator() {
    return new PathIterator()
    {
      @Override
      public void close() {
        // do nothing
      }

      @Override
      public boolean hasNext() {
        return true;
      }

      @Override
      public String next() {
        return prefix + "/" + System.nanoTime();
      }
    };
  }
}
