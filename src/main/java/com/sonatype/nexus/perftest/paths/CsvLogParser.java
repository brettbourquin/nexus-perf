/*
 * Copyright (c) 2007-2013 Sonatype, Inc. All rights reserved.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0,
 * which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html.
 */
package com.sonatype.nexus.perftest.paths;

import java.io.File;
import java.util.StringTokenizer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CsvLogParser
    extends FileParser
    implements DownloadPaths
{
  @JsonCreator
  public CsvLogParser(final @JsonProperty(value = "logfile", required = true) File logfile) {
    super(logfile);
  }

  @Override
  protected String parseLine(final String line) {
    StringTokenizer st = new StringTokenizer(line, ",");
    return st.nextToken();
  }
}
