package com.sonatype.nexus.perftest.paths;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;

import com.google.common.base.Throwables;
import org.apache.commons.io.LineIterator;

public abstract class FileParser
    implements DownloadPaths
{
  private final File logfile;

  public FileParser(final File logfile)
  {
    this.logfile = logfile;
  }

  protected boolean isValidLine(final String line) {
    return true;
  }

  protected abstract String parseLine(final String line);

  @Override
  public PathIterator iterator() {
    return new Iterator();
  }

  @Override
  public PathIterator circularIterator() {
    return new Iterator()
    {
      @Override
      public boolean hasNext() {
        return true;
      }

      @Override
      public String next() {
        if (!super.hasNext()) {
          close();
          logIterator = createLogIterator();
        }
        return super.next();
      }
    };
  }

  private class Iterator
      implements PathIterator
  {
    LineIterator logIterator;

    private Iterator() {
      logIterator = createLogIterator();
    }

    LineIterator createLogIterator() {
      try {
        InputStream in = new FileInputStream(logfile);
        try {
          in = new GZIPInputStream(in);
        }
        catch (Exception e) {
          // probably not a gzip
          in.close();
          in = new FileInputStream(logfile);
        }
        return new LineIterator(new InputStreamReader(in))
        {
          @Override
          protected boolean isValidLine(final String line) {
            return FileParser.this.isValidLine(line);
          }
        };
      }
      catch (IOException e) {
        throw Throwables.propagate(e);
      }
    }

    @Override
    public void close() {
      logIterator.close();
    }

    @Override
    public boolean hasNext() {
      return logIterator.hasNext();
    }

    @Override
    public String next() {
      return parseLine(logIterator.next());
    }
  }


}
