package com.sonatype.nexus.perftest.helm;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;
import java.util.zip.GZIPOutputStream;

import com.sonatype.nexus.perftest.operation.UploadEntities;
import com.sonatype.nexus.perftest.operation.UploadEntity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Charsets;
import com.google.common.base.Throwables;
import com.google.common.io.ByteStreams;
import com.google.common.io.Resources;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;

/**
 * Generates a specified number of charts and loops through them.
 */
public class ChartGenerator
    implements UploadEntities
{
  private final String chartYamlTemplate;

  private final Integer numberOfCharts;

  private final int minSize;

  private final int maxSize;

  public ChartGenerator(@JsonProperty(value = "numberOfCharts", defaultValue = "100") Integer numberOfCharts,
                        @JsonProperty(value = "minSize", defaultValue = "1") Integer minSize,
                        @JsonProperty(value = "maxSize", defaultValue = "10") Integer maxSize) throws IOException
  {
    this.numberOfCharts = nvl(numberOfCharts, 100);

    // min/max are in Kb
    this.minSize = nvl(minSize, 1) * 1024;
    this.maxSize = nvl(maxSize, 10) * 1024;

    chartYamlTemplate = Resources.toString(getClass().getResource("Chart.yaml"), Charsets.UTF_8);
  }

  private InputStream generateChart(final String name, final String version) {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();

    try (TarArchiveOutputStream out = new TarArchiveOutputStream(new GZIPOutputStream(baos))) {
      out.setBigNumberMode(TarArchiveOutputStream.BIGNUMBER_STAR);
      out.setLongFileMode(TarArchiveOutputStream.LONGFILE_GNU);

      byte[] chart = chartYamlTemplate.replace("${name}", name).replace("${version}", version).getBytes(Charsets.UTF_8);

      // add Chart.yaml that will match the name/version
      TarArchiveEntry chartYamlEntry = new TarArchiveEntry("/" + name + "/Chart.yaml");
      chartYamlEntry.setSize(chart.length);
      out.putArchiveEntry(chartYamlEntry);
      ByteStreams.copy(new ByteArrayInputStream(chart), out);
      out.closeArchiveEntry();

      // add a random data entry of a random size
      int size = ThreadLocalRandom.current().nextInt(minSize, maxSize + 1);
      TarArchiveEntry dataEntry = new TarArchiveEntry("/" + name + "/data");
      dataEntry.setSize(size);
      out.putArchiveEntry(dataEntry);
      InputStream randomStream = new InputStream()
      {
        private int count;

        @Override
        public int read() throws IOException {
          if (count == size) {
            return -1;
          }
          count++;
          return ThreadLocalRandom.current().nextInt(0, 256);
        }
      };
      ByteStreams.copy(randomStream, out);
      out.closeArchiveEntry();
    }
    catch (IOException e) {
      throw Throwables.propagate(e);
    }
    return new ByteArrayInputStream(baos.toByteArray());
  }

  @Override
  public Iterator<UploadEntity> iterator() {
    return new Iterator<UploadEntity>()
    {
      @Override
      public boolean hasNext() {
        return true;
      }

      @Override
      public UploadEntity next() {
        String name = "generator";
        String version = "0.0." + ThreadLocalRandom.current().nextInt(1, numberOfCharts + 1);
        return new UploadEntity(chartPath(name, version), () -> generateChart(name, version));
      }
    };
  }

  private static String chartPath(final String name, final String version) {
    return String.format("/%1$s/%2$s/%1$s-%2$s.tgz", name, version);
  }

  private static int nvl(final Integer value, final int defaultValue) {
    return value == null ? defaultValue : value;
  }

}
