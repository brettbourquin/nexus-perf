package com.sonatype.nexus.perftest.helm;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.sonatype.nexus.perftest.Nexus;
import com.sonatype.nexus.perftest.operation.CircularIterator;
import com.sonatype.nexus.perftest.paths.DownloadPaths;

import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Throwables;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.yaml.snakeyaml.Yaml;

/**
 * Reads index.yaml from specified Nexus repository and extracts each published chart as a download path.
 * Every time all the charts are iterated, the process of reading the index/parse is repeated. In this way, eventual
 * new charts present in the repository are found.
 */
public class IndexParser
    implements DownloadPaths
{
  private static final Yaml yaml = new Yaml();

  private final String baseUrl;

  private final CloseableHttpClient httpClient;

  public IndexParser(@JacksonInject Nexus nexus,
                     @JsonProperty("preemptiveAuth") boolean preemptiveAuth,
                     @JsonProperty(value = "pathPrefix", required = true) String pathPrefix)
  {
    String baseUrl = Nexus.sanitizeUrl(nexus.getBaseurl()) + (pathPrefix == null ? "" : pathPrefix);
    this.baseUrl = (baseUrl.endsWith("/") ? baseUrl : baseUrl + "/");

    httpClient = Nexus.createHttpClient(nexus, preemptiveAuth);
  }


  @Override
  public PathIterator iterator() {
    throw new RuntimeException(getClass().getSimpleName() + "#iterator() cannot be invoked");
  }

  @Override
  public PathIterator circularIterator() {
    CircularIterator<String> circularIterator = new CircularIterator<String>(new ArrayList<>())
    {
      private ReadWriteLock lock = new ReentrantReadWriteLock();

      @Override
      protected String getElement(final List<String> elements, final int index) {
        if (index == 0) {
          lock.writeLock().lock();
          try {
            HttpGet request = new HttpGet(baseUrl + "/index.yaml");
            CloseableHttpResponse response = httpClient.execute(request);
            if (response.getStatusLine().getStatusCode() != 200) {
              EntityUtils.consume(response.getEntity());
              throw new RuntimeException("Could not retrieve index.yaml from " + baseUrl);
            }
            try (InputStream in = response.getEntity().getContent()) {
              Map<String, Object> indexYaml = (Map<String, Object>) yaml.load(in);
              elements.clear();
              Map<String, List<Map<String, Object>>> entries = (Map) indexYaml.get("entries");
              if (entries != null) {
                for (List<Map<String, Object>> charts : entries.values()) {
                  for (Map<String, Object> chart : charts) {
                    List<String> urls = (List<String>) chart.get("urls");
                    if (urls != null && urls.size() > 0) {
                      String url = new URL(urls.get(0)).toExternalForm();
                      if (url.startsWith(baseUrl)) {
                        url = url.substring(baseUrl.length());
                      }
                      elements.add(url);
                    }
                  }
                }
              }
              Collections.shuffle(elements);
            }
          }
          catch (Exception e) {
            Throwables.propagateIfPossible(e);
            throw new RuntimeException("Could not retrieve index.yaml from " + baseUrl, e);
          }
          finally {
            lock.writeLock().unlock();
          }
        }

        lock.readLock().lock();
        try {
          return super.getElement(elements, index);
        }
        finally {
          lock.readLock().unlock();
        }
      }

      @Override
      public Iterable<String> getAll() {
        lock.readLock().lock();
        try {
          return super.getAll();
        }
        finally {
          lock.readLock().unlock();
        }
      }

      @Override
      public int getSize() {
        lock.readLock().lock();
        try {
          return super.getSize();
        }
        finally {
          lock.readLock().unlock();
        }
      }
    };
    return new PathIterator()
    {
      @Override
      public void close() throws IOException {
        //do nothing
      }

      @Override
      public boolean hasNext() {
        return circularIterator.getSize() > 0;
      }

      @Override
      public String next() {
        return circularIterator.getNext();
      }
    };
  }
}
