# Docker image for Performance Client

This folder contains "dockerfile" to build docker image with our distribution.

## To build

Execute command:

```
$ mvn clean package docker:build
```

## To run

Execute command to run as remote agent:

```
$ docker run -d -p <PORT>:<PORT> -e PORT=<PORT> -v <PATH TO SCENARIOS ROOT>:/opt/takari/nexus-perf-scenarios takari/nexus-perf
```

Execute command to run a scenario:

```
$ docker run -d -p <PORT>:<PORT> -e PORT=<PORT> -v <PATH TO SCENARIOS ROOT>:/opt/takari/nexus-perf-scenarios takari/nexus-perf <SCENARIO>
```
where:
  * PORT: the JMX port to listen to
  * PATH TO SCENARIOS ROOT: local root directory containing scenarios 
  * SCENARIO: scenario to run (directory name). e.g. maven01

