#!/bin/bash

echo ${JAVA_HOME}/bin/java \
       -server \
       -Djava.net.preferIPv4Stack=true \
       -Dcom.sun.management.jmxremote.port=${PORT} \
       -Dcom.sun.management.jmxremote.rmi.port=${PORT} \
       -Djava.rmi.server.hostname=${HOST} \
       -Dcom.sun.management.jmxremote.authenticate=false \
       -Dcom.sun.management.jmxremote.ssl=false \
       -jar /opt/takari/nexus-perf/${project.build.finalName}-jar-with-dependencies.jar $1

exec ${JAVA_HOME}/bin/java \
  -server \
  -Djava.net.preferIPv4Stack=true \
  -Dcom.sun.management.jmxremote.port=${PORT} \
  -Dcom.sun.management.jmxremote.rmi.port=${PORT} \
  -Djava.rmi.server.hostname=${HOST} \
  -Dcom.sun.management.jmxremote.authenticate=false \
  -Dcom.sun.management.jmxremote.ssl=false \
  -jar /opt/takari/nexus-perf/${project.build.finalName}-jar-with-dependencies.jar $1