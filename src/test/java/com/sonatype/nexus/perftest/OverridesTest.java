package com.sonatype.nexus.perftest;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OverridesTest {

    @Test
    public void test() throws Exception {
        System.setProperty("test.rate", "1/SECOND");
        System.setProperty("test.my.rate", "10/SECOND");

        System.setProperty("test.numberOfClients", "0");
        System.setProperty("test.my.numberOfClients", "100");

        Overrides o = new Overrides();
        assertEquals(1000, o.getRate("someone's else", null).getPeriodMillis());
        assertEquals(100, o.getRate("my", null).getPeriodMillis());

        assertEquals(0, o.getNumberOfClients("xyz", 200));
        assertEquals(100, o.getNumberOfClients("my", 200));
    }
}
